import urllib.request
from tkinter import *
import random
import time
import math

root = Tk()
canvas = Canvas(root, width=730, height=170, bg="gray")

flatPlanCoord = [] # координаты для рандомного выбора точки начала работы пылесоса
wallsCoord = [] # координаты стен (блоки из квадратов 10х10)
currentPoint = []# текущая точка положения пылесоса
vacuumedPoints = [] # пропылесошеные точки
dirtPoints = [] # ещё не убранные точки, записанные пылесосом
lastPoint = []

def drawSquare(g,h,color): # отрисовка квадрата стены или пропылесошеной зоны
    global flatPlanCoord
    global dirtPoints
    global lastPoint
    global currentPoint
    if [g,h] in dirtPoints: dirtPoints.remove([g,h]) # убираем точку из списка, если она там есть
    if color=="white": lastPoint = [g,h]
    if color=="red":
        currentPoint = [g,h]
        if [g,h] not in vacuumedPoints: vacuumedPoints.append([g,h])
    canvas.create_rectangle(g,h,g+10,h+10, fill=color)
    canvas.update()

def drawFlatPlan(data): # отрисока плана помещения
    global wallsCoord
    global flatPlanCoord
    y=0
    for line in data:
        x=0
        for i in line:
            if i=="#":
                drawSquare(x,y,"black") # стена
                wallsCoord.append([x,y])
            else: flatPlanCoord.append([x,y])
            x+=10
        y+=10

def readFlatPlan(url):
    flat = []
    with urllib.request.urlopen(url) as response:
        for line in response:
            flat.append(line.decode("utf-8").replace("\n",""))
    return flat

def goToRandomPoint():
    global currentPoint
    currentPoint = flatPlanCoord[random.randint(0,len(flatPlanCoord)-1)]
    drawSquare(currentPoint[0],currentPoint[1],"red")

def squareCheck(coordinates):
    if coordinates in wallsCoord or coordinates in vacuumedPoints: return False
    else: return True

def stepForward(x,y,redX, redY, checkXLeft, checkYLeft, checkXRight, checkYRight):
    global currentPoint
    global dirtPoints

    time.sleep(0.05)
    drawSquare(x,y,"white")
    currentPoint = [redX,redY] # меняем текущую точку
    drawSquare(redX,redY,"red") # рисуем пылесос
    if squareCheck([checkXLeft,checkYLeft]): dirtPoints.append([checkXLeft,checkYLeft])
    if squareCheck([checkXLeft,checkYLeft])==False and squareCheck([checkXRight,checkYRight])==False:
        return False #если точки вокруг пропылесошены или являются стеной

def rigthStep():
    global currentPoint
    global dirtPoints
    while squareCheck([currentPoint[0]+10,currentPoint[1]]): # если пусто (шаг направо)
        x,y = currentPoint[0],currentPoint[1]
        stepForward(x,y,x+10,y,x+10,y-10, x+10, y+10)
        if squareCheck([x+10,y+10]):
            downStep()

def upStep():
    global currentPoint
    while squareCheck([currentPoint[0],currentPoint[1]-10]): # если пусто (шаг наверх)
        x,y = currentPoint[0],currentPoint[1]
        stepForward(x,y,x,y-10,x-10,y-10, x+10, y-10)
        if squareCheck([x+10,y-10]):
            rigthStep()

def leftStep():
    global currentPoint
    while squareCheck([currentPoint[0]-10,currentPoint[1]]):
        x,y = currentPoint[0],currentPoint[1]
        stepForward(x,y,x-10,y,x-10,y+10, x-10, y-10)
        if squareCheck([x-10,y-10]):
            upStep()

def downStep():
    global currentPoint
    while squareCheck([currentPoint[0],currentPoint[1]+10]):
        x,y = currentPoint[0],currentPoint[1]
        stepForward(x,y,x,y+10,x+10,y+10,x-10,y+10)
        if squareCheck([x-10,y+10]):
            leftStep()

def vacuuming():
    deadEnd = True
    while deadEnd:
        deadEnd = upStep()
        deadEnd = leftStep()
        deadEnd = downStep()
        deadEnd = rigthStep()

def startVacuuming():
    global currentPoint
    global dirtPoints
    if squareCheck([currentPoint[0],currentPoint[1]-10]): dirtPoints.append([currentPoint[0],currentPoint[1]-10]) # проверка точки сверху
    if squareCheck([currentPoint[0],currentPoint[1]+10]): dirtPoints.append([currentPoint[0],currentPoint[1]+10]) # проверка точки снизу
    if squareCheck([currentPoint[0]-10,currentPoint[1]]): dirtPoints.append([currentPoint[0]-10,currentPoint[1]]) # проверка точки слева
    
    while squareCheck([currentPoint[0]+10,currentPoint[1]]): # если пусто (шаг направо)
        x,y = currentPoint[0],currentPoint[1]
        stepForward(x,y,x+10,y,x+10,y-10,x+10,y+10)
        if squareCheck([x+10,y+10]): dirtPoints.append([x+10,y+10])

def choiseNearPoint(x,y,xDirt,yDirt): # поиск ближайшей точки к текущему положению и искомой грязной точке
    global vacuumedPoints
    global lastPoint
    nearPoints = []
    a=math.inf
    for coordinates in vacuumedPoints:
        if coordinates[0]==x and (coordinates[1]==y+10 or coordinates[1]==y-10): nearPoints.append(coordinates)
        if (coordinates[0]==x+10 or coordinates[0]==x-10) and coordinates[1]==y: nearPoints.append(coordinates)
    if len(nearPoints)>1:
        if lastPoint in nearPoints: nearPoints.remove(lastPoint)
        points = []
        print ("!!! lastPoint", lastPoint)
        for i in nearPoints:
            points.append(abs(xDirt-i[0])+abs(yDirt-i[1]))
            print ("!!! abs(xDirt-i[0])+abs(yDirt-i[1]):", abs(xDirt-i[0])," + ",abs(yDirt-i[1]))
        for i in points:
            if i<a: a=i
        nearPointIndex=points.index(a)
        print ("!!! Current point:", x," ",y)
        print ("!!! Dirt point:", xDirt," ",yDirt)
        print ("!!! nearPoints:",nearPoints)
        print ("!!! nearPoints[nearPointIndex]:",nearPoints[nearPointIndex])
        return nearPoints[nearPointIndex]
    else:
        print("nearPoints==1:", nearPoints)
        return nearPoints[0]


def searchDirtPoint():
    global currentPoint
    global dirtPoints
    lastDirtPoint = dirtPoints[len(dirtPoints)-1]
    xDirt,yDirt= lastDirtPoint[0],lastDirtPoint[1]
    searchNotComplete = True
    print ("Last dirt point",lastDirtPoint)

    while searchNotComplete:
        time.sleep(0.05)
        drawSquare(currentPoint[0],currentPoint[1],"white")
        currentPoint = choiseNearPoint(currentPoint[0],currentPoint[1],xDirt,yDirt)
        print ("Current point into searchNotComplite", currentPoint)
        c,d = currentPoint[0],currentPoint[1]
        drawSquare(c,d,"red")
        if xDirt==c and yDirt==d+10:
            time.sleep(0.05)
            drawSquare(c,d,"white")
            currentPoint = [c,d+10]
            drawSquare(c,d+10,"red")
            searchNotComplete = False
        elif xDirt==c and yDirt==d-10:
            time.sleep(0.1)
            drawSquare(c,d,"white")
            currentPoint = [c,d-10]
            drawSquare(c,d-10,"red")
            searchNotComplete = False
        elif xDirt==c+10 and yDirt==d:
            time.sleep(0.1)
            drawSquare(c,d,"white")
            currentPoint = [c+10,d]
            drawSquare(c+10,d,"red")
            searchNotComplete = False
        elif xDirt==c-10 and yDirt==d:
            time.sleep(0.1)
            drawSquare(c,d,"white")
            currentPoint = [c-10,d]
            drawSquare(c-10,d,"red")
            searchNotComplete = False

def start(event):
    global dirtPoints
    startVacuuming()
    while len(dirtPoints)>0:
        vacuuming() # пылесосим до тех пор, пока вокрег есть пустые точки
        searchDirtPoint()

def exit(event):
    root.destroy()

flat = readFlatPlan('http://saask.ee/examples/room.txt')
drawFlatPlan(flat) # рисуем план помещения
goToRandomPoint() # помещаем пылесос в случайную точку квартиры

button1=Button(root, text='включить пылесос',width=25,height=2,bg='black',fg='red',font='arial 14')
button1.bind('<1>', start)
button2=Button(root, text='exit',width=25,height=2,bg='black',fg='red',font='arial 14')
button2.bind('<1>', exit)
button1.pack()
button2.pack()
canvas.pack()
root.mainloop()